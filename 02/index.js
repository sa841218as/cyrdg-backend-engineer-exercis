module.exports = function pipe(value, ...funcs) {
  return funcs.reduce((prevValue, func) => {
    return func(prevValue)
  }, value)
}