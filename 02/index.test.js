const pipe = require('./index')
const { expect } = require('chai')

function add(val) {
  return val + 1
}

function minus(val) {
  return val - 1
}

function concatStrA(val) {
  return val + 'A'
}

function concatStrB(val) {
  return val + 'B'
}

function concatStrC(val) {
  return val + 'C'
}

describe('#02 Unit Test', () => {
  it('未輸入 function parameter，返回原值', () => {
    expect(pipe(10)).to.be.equal(10)
  })

  it('傳入一個 function', () => {
    expect(pipe(10, add)).to.be.equal(11)
  })

  it('傳入多個 function', () => {
    expect(pipe(10, add, add, add, minus, add)).to.be.equal(13)
  })

  it('驗證function 執行順序', () => {
    expect(pipe("", concatStrA, concatStrB, concatStrC, concatStrB)).to.be.equal("ABCB")
  })
})
