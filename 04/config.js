module.exports = {
  siteURL: 'https://www.alexa.com/topsites/',

  topsites: {
    rowClassName: 'site-listing'
  }
}