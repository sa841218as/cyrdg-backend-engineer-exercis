const axios = require('axios')
const cheerio = require('cheerio')
const { siteURL, topsites } = require('../config')

module.exports = function (count = 10) {
  axios.get(siteURL)
    .then(res => {
      const body = res.data
      const $ = cheerio.load(body)

      $(`.${topsites.rowClassName}`).slice(0, count).each(function (index) {
        console.log(`${index + 1}. ${$(this).find('a').text()}`)
      })
    })
    .catch(err => {
      console.error(err.toString())
    })
}