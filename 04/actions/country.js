const axios = require('axios')
const cheerio = require('cheerio')
const { siteURL, topsites } = require('../config')

module.exports = function (country) {
  if (!country) {
    console.error('Error usage：Please offer country name！')
    return
  }
  
  country = country.toLowerCase()
  axios.get(`${siteURL}/countries`)
    .then(res => {
      const body = res.data
      const $ = cheerio.load(body)
      let subPath = null
      
      $('.countries a').each(function () {
        if ($(this).text().toLowerCase() === country) {
          subPath = $(this).attr('href')
          return false
        }
      })

      return subPath ? 
        axios.get(`${siteURL}${subPath}`) : 
        Promise.reject('Country not found')
    })
    .then(res => {
      const body = res.data
      const $ = cheerio.load(body)

      $(`.${topsites.rowClassName}`).slice(0, 20).each(function (index) {
        console.log(`${index + 1}. ${$(this).find('a').text()}`)
      })
    })
    .catch(err => {
      console.error(err.toString())
    })
}