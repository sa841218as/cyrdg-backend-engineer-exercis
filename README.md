# 創弈人數位科技股份有限公司 Backend Engineer Exercise
> 試題皆以 Node.js 撰寫  
> 建議 Node 版本為 v14.15.4  

## 安裝依賴套件
```shell
$ npm install
```

## 測試報告
```shell
$ npm test
```
