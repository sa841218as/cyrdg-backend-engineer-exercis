const f = require('./index')
const { expect } = require('chai')

describe('#01 Unit Test', () => {
  it('輸入為0', () => {
    expect(f(0)).to.be.equal("0")
  })

  it('正整數(小於三位數)', () => {
    expect(f(123)).to.be.equal("123")
  })

  it('正整數(大於三位數)', () => {
    expect(f(1234567)).to.be.equal("1,234,567")
  })

  it('帶有小數點(小於三位數)', () => {
    expect(f(123.23456)).to.be.equal("123.23456")
  })

  it('帶有小數點(大於三位數)', () => {
    expect(f(-1234567.23456)).to.be.equal("-1,234,567.23456")
  })

  it('小數點為0時省略', () => {
    expect(f(123456.0000)).to.be.equal("123,456")
  })

  it('輸入為1000', () => {
    expect(f(1000)).to.be.equal("1,000")
  })

  it('輸入為-1000', () => {
    expect(f(-1000)).to.be.equal("-1,000")
  })

  it('輸入為1000000', () => {
    expect(f(1000000)).to.be.equal("1,000,000")
  })

  it('輸入為-1000000', () => {
    expect(f(-1000000)).to.be.equal("-1,000,000")
  })

  it('輸入為1000000000', () => {
    expect(f(1000000000)).to.be.equal("1,000,000,000")
  })

  it('輸入為-1000000000', () => {
    expect(f(-1000000000)).to.be.equal("-1,000,000,000")
  })

  it('輸入類型非 Number，返回 NaN', () => {
    expect(isNaN(f("sfwfdw"))).to.be.equal(true)
  })
})
