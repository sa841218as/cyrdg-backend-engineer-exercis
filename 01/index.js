module.exports = function (number) {
  if (isNaN(number))
    return NaN
  else if (number < 1000 && number > -1000)
    return number.toString()

  const sign = Math.sign(number)
  number *= sign
  let [integer, decimal] = number.toString().split('.')
  let arr = []

  while (integer) {
    const num = integer.substr(-3)
    arr = [num, ...arr]
    integer = integer.substr(0, integer.length - num.length)
  }

  return `${sign === -1 ? '-' : ''}${arr.join(',')}${decimal ? `.${decimal}` : ''}`
}